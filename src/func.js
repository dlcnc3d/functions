const getSum = (str1, str2) => {
  if (
    typeof str1 !== "string" ||
    typeof str2 !== "string" ||
    str1.replace(/\D/g, "").length !== str1.length ||
    str2.replace(/\D/g, "").length !== str2.length
  ) {
    return false;
  }

  let a = str1
    .split("")
    .reverse()
    .map((x) => {
      return x !== "" ? parseInt(x) : 0;
    });
  let b = str2
    .split("")
    .reverse()
    .map((x) => {
      return x !== "" ? parseInt(x) : 0;
    });
  let result = [];
  let additionElement = 0;

  for (let i = 0; i < (a.length > b.length ? a.length : b.length); i++) {
    result.unshift((a[i] || 0) + (((b[i] || 0) + additionElement) % 10));
    additionElement = (a[i] || 0) + ((b[i] || 0) + additionElement) > 9 ? 1 : 0;
  }

  if (additionElement === 1) {
    result.unshift(additionElement);
  }

  return result.join("");
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  let countPost = listOfPosts.reduce((countP, a) => {
    if (a.author === authorName) return countP + 1;
    else return countP;
  }, 0);

  let countComents = listOfPosts
    .filter((x) => {
      return x.comments?.length > 0;
    })
    .map((x) => x.comments)
    .reduce((totalCount, element) => {
      return (
        totalCount +
        element.reduce((count, x) => {
          if (x.author === authorName) return count + 1;
          else return count;
        }, 0)
      );
    }, 0);

  return "Post:" + countPost + ",comments:" + countComents;
};

const tickets = (people) => {
  for (let i = 0; i < people.length; i++) {
    if (people[0] > 25) {
      return "NO";
    }
    if (people[i] - 25 > 25 * i) {
      return "NO";
    }
  }
  return "YES";
};

module.exports = { getSum, getQuantityPostsByAuthor, tickets };
